import Foundation

class MyCityListViewModel: MyCityListViewProtocol {

    // MARK: - Propertis
    private(set)var cities: [MyCity] = []
    private var indexPath: IndexPath?

    // MARK: - Network functions
    func fetchCity(city: String, completion: @escaping () -> Void) {
        WeatherNetworkService.shared.fetchCurrentWeather(cityName: city) { (currentWeather, error) in
            if let error = error {
                print(error)
                return
            }
            WeatherNetworkService.shared.fetchDailyWeather(cityName: city) { (dailyWeather, error) in
                if let error = error {
                    print(error)
                    return
                }
                let myCity = MyCity(name: city, dailyWeather: dailyWeather, currentWeather: currentWeather)

                if self.cities.contains(where: { (city) -> Bool in
                    return city.name.contains(myCity.name)
                }) {
                    if let row = self.cities.firstIndex(where: {$0.name == myCity.name}) {
                        self.cities[row].currentWeather = myCity.currentWeather
                    }
                    completion()
                } else {
                    self.cities.append(myCity)
                    completion()
                }
            }
        }
    }

    // MARK: - Cell functions
    func selectRow(for indexPath: IndexPath) {
        self.indexPath = indexPath
    }

    func numberOfRows() -> Int {
        return cities.count
    }

    func cellViewModel(for indexPath: IndexPath) -> MyCityListCellViewProtocol? {
        let city = cities[indexPath.row]
        return MyCityListCellViewModel(city: city)
    }

    func viewModelForSelectedRow() -> DetailViewModelProtocol? {
        guard let indexPath = indexPath else { return nil }
        let city = cities[indexPath.row]
        return DetailViewModel(city: city)
    }
}
