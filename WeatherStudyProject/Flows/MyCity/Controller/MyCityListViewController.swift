import UIKit

class MyCityListViewController: UIViewController, UINavigationControllerDelegate {

    // MARK: - Outlets
    private var refreshControler: UIRefreshControl = {
        let refreshControler = UIRefreshControl()
        refreshControler.backgroundColor = .white
        refreshControler.tintColor = .black
        refreshControler.frame = CGRect.init(x: 0, y: 0, width: 50, height: 50)
        refreshControler.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        return refreshControler
    }()
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private var addCityButton: CustomButton = {
        let button = CustomButton(widthAndHeight: 70, image: #imageLiteral(resourceName: "addCity"))
        button.addTarget(self, action: #selector(handleAddCity), for: .touchUpInside)
        return button
    }()

    // MARK: - Properties
    private var viewModel: MyCityListViewProtocol! {
        didSet {
            self.viewModel.fetchCity(city: "Moscow") {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    var onAddCity: (() -> Void)?
    var onDetailCity: ((_ viewModel: DetailViewModelProtocol?) -> Void)?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        viewModel = MyCityListViewModel()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }

    // MARK: - Actions
    @objc private func handleAddCity() {
        let controller = AddCityViewController()
        controller.returnCityName = { [weak self] cityName in
            self?.viewModel.fetchCity(city: cityName) {
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
        navigationController?.pushViewController(controller, animated: true)
    }

    @objc private func handleRefresh(sender: UIRefreshControl) {
        viewModel.cities.forEach { (city) in
            self.viewModel.fetchCity(city: city.name) {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
        sender.endRefreshing()
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension MyCityListViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: MyCityListTableViewCell.reuseId,
            for: indexPath) as? MyCityListTableViewCell else {
                return UITableViewCell()
        }
        let cellViewModel = viewModel.cellViewModel(for: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectRow(for: indexPath)
        let detailsViewModel = viewModel.viewModelForSelectedRow()
        onDetailCity?(detailsViewModel)
    }
}

// MARK: - Setup UI
extension MyCityListViewController {
    private func setupUI() {
        tableView.addSubview(refreshControler)
        view.addSubview(tableView)
        tableView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.backgroundColor = .black

        tableView.register(MyCityListTableViewCell.self, forCellReuseIdentifier: MyCityListTableViewCell.reuseId)

        view.addSubview(addCityButton)
        addCityButton.anchor(top: nil,
                             leading: nil,
                             bottom: view.safeAreaLayoutGuide.bottomAnchor,
                             trailing: view.trailingAnchor,
                             padding: .init(top: 0, left: 0, bottom: 40, right: 16))
    }
}
