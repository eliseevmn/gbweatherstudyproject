import UIKit

class MyCityListTableViewCell: UITableViewCell {

    // MARK: - Properties
    static let reuseId = "MyCityListTableViewCell"

    var viewModel: MyCityListCellViewProtocol! {
        didSet {
            cityNameLabel.text = viewModel.cityName
            currentTempLabel.text = "\(viewModel.currentTemp ?? "")"
            currentTimeLabel.text = "\(viewModel.currentTime ?? "")"

            iconWeather.set(iconString: viewModel.weatherIcon)
        }
    }

    // MARK: - Outlets
    private let cityNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return label
    }()
    private let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()
    private let currentTempLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 30, weight: .medium)
        return label
    }()
    private var iconWeather: WebImageView = {
        let imageView = WebImageView()
        return imageView
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func configureUI() {
        iconWeather.heightAnchor.constraint(equalToConstant: 60).isActive = true
        iconWeather.widthAnchor.constraint(equalToConstant: 60).isActive = true

        let verticalStackView = UIStackView(arrangedSubviews: [
            currentTimeLabel,
            cityNameLabel
        ])
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .leading
        verticalStackView.spacing = 10
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconWeather,
            verticalStackView,
            currentTempLabel
        ])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.anchor(top: topAnchor,
                                   leading: leadingAnchor,
                                   bottom: bottomAnchor,
                                   trailing: trailingAnchor,
                                   padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }
}
