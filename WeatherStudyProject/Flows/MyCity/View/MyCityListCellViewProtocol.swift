import Foundation

protocol MyCityListCellViewProtocol {
    var cityName: String? { get }
    var currentTime: String? { get }
    var currentTemp: String? { get }
    var weatherIcon: String? { get }
    init(city: MyCity)
}
