import Foundation

protocol DetailCellViewProtocol {
    var date: String? { get }
    var weatherIcon: String? { get }
    var temperature: String? { get }
    var description: String? { get }
    init(dailyWeather: DailyList)
}
