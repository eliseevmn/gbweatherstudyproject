import UIKit

class CurrentView: UIView {

    // MARK: - Properties
    var cityWeather: MyCity?

    // MARK: - Outlets
    private let cityNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        return label
    }()

    private let temperatureLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 40, weight: .medium)
        label.textAlignment = .center
        return label
    }()

    private let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .light)
        label.textAlignment = .center
        return label
    }()

    private let iconWeather: WebImageView = {
        let iconWeather = WebImageView()
        iconWeather.heightAnchor.constraint(equalToConstant: 100).isActive = true
        iconWeather.widthAnchor.constraint(equalToConstant: 100).isActive = true
        return iconWeather
    }()

    // MARK: - Init
    init(cityWeather: MyCity?) {
        self.cityWeather = cityWeather
        super.init(frame: .zero)
        configureData()
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func configureData() {
        cityNameLabel.text = cityWeather?.name
        descriptionLabel.text = cityWeather?.currentWeather?.weather[0].weatherDescription

        iconWeather.set(iconString: cityWeather?.currentWeather?.weather[0].icon)

        guard let tempFromJson = cityWeather?.currentWeather?.main.temp else {
            temperatureLabel.text = "Информация отсутствует"
            return
        }
        temperatureLabel.text = String(format: "%.0f℃", tempFromJson - 273.15)
    }

    private func setupUI() {
        let verticalImageStackView = UIStackView(arrangedSubviews: [
            iconWeather
        ])
        verticalImageStackView.axis = .vertical
        verticalImageStackView.spacing = 10

        let verticalInfoView = UIStackView(arrangedSubviews: [
            cityNameLabel,
            temperatureLabel,
            descriptionLabel
        ])
        verticalInfoView.axis = .vertical
        verticalInfoView.spacing = 20

        let overlayStackView = UIStackView(arrangedSubviews: [
            verticalImageStackView,
            verticalInfoView
        ])
        overlayStackView.spacing = 20
        overlayStackView.alignment = .center
        addSubview(overlayStackView)
        overlayStackView.fillSuperview(padding: .init(top: 40,
                                                       left: 16,
                                                       bottom: 40,
                                                       right: 16))
    }
}
