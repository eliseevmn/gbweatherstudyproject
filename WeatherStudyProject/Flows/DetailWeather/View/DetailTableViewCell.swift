import UIKit

class DetailTableViewCell: UITableViewCell {

    // MARK: - Properties
    static let reuseId = "DetailTableViewCell"

    var viewModel: DetailCellViewProtocol! {
        didSet {
            dateLabel.text = viewModel.date
            temperatureLabel.text = viewModel.temperature
            descriptionLabel.text = viewModel.description
            iconWeather.set(iconString: viewModel.weatherIcon)
        }
    }

    // MARK: - Outlets
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        return label
    }()

    private var iconWeather: WebImageView = {
        let imageView = WebImageView()
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        return imageView
    }()

    private var temperatureLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .regular)
        return label
    }()

    private var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        return label
    }()

    // MARK: - Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .white
        configureUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Setup UI
    private func configureUI() {
        let verticalStackView = UIStackView(arrangedSubviews: [
            dateLabel,
            temperatureLabel
        ])
        verticalStackView.axis = .vertical
        verticalStackView.alignment = .leading
        verticalStackView.spacing = 10
        let horizontalStackView = UIStackView(arrangedSubviews: [
            iconWeather,
            verticalStackView,
            UIView(),
            descriptionLabel
        ])
        horizontalStackView.axis = .horizontal
        horizontalStackView.spacing = 10
        horizontalStackView.alignment = .center
        addSubview(horizontalStackView)
        horizontalStackView.anchor(top: topAnchor,
                                   leading: leadingAnchor,
                                   bottom: bottomAnchor,
                                   trailing: trailingAnchor,
                                   padding: .init(top: 10, left: 16, bottom: 10, right: 16))
    }
}
