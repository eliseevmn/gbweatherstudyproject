import Foundation

protocol AddCityCellViewModelProtocol {
    var cityName: String? { get }
    init(city: CityResponse)
}
