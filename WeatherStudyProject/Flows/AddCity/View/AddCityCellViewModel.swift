import Foundation

class AddCityCellViewModel: AddCityCellViewModelProtocol {

    private let city: CityResponse

    var cityName: String? {
        return city.name
    }

    required init(city: CityResponse) {
        self.city = city
    }
}
