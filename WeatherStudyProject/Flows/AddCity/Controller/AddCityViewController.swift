import UIKit

class AddCityViewController: UIViewController {

    // MARK: - Propertries
    private var viewModel: AddCityViewModelProtocol!
    private var timer: Timer?
    private var tableView: UITableView = UITableView(frame: .zero, style: .plain)
    private let searchController = UISearchController(searchResultsController: nil)
    var returnCityName: ((String) -> Void)?
//    var onCityList: ((String) -> Void)?

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        viewModel = AddCityViewModel()
        setupUI()
        setupSearchController()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension AddCityViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cities.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: AddCityTableViewCell.reuseId,
            for: indexPath) as? AddCityTableViewCell else {
                return UITableViewCell()
        }

        let cellViewModel = viewModel.cellViewModel(for: indexPath)
        cell.viewModel = cellViewModel

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectRow(for: indexPath)
        let city = viewModel.viewModelForSelectedRow()

        guard let completion = returnCityName else {return}
        completion(city?.name ?? "")
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - UISearchBarDelegate, UISearchResultsUpdating
extension AddCityViewController: UISearchBarDelegate, UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text, query.count > 2 else {
            return
        }
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [weak self] (_) in

            self?.viewModel.fetchCity(searchCity: query) {
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        })
    }
}

// MARK: - Setup UI
extension AddCityViewController {
    private func setupUI() {
        view.addSubview(tableView)
        tableView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         bottom: view.bottomAnchor,
                         trailing: view.trailingAnchor)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

        tableView.register(AddCityTableViewCell.self, forCellReuseIdentifier: AddCityTableViewCell.reuseId)

        navigationItem.title = "Поиск"
    }

    private func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.searchBar.placeholder = "Search City"
        searchController.searchBar.delegate = self
    }
}
