import Foundation
import Moya

protocol Networkable {
    var weatherNetworkManager: MoyaProvider<WeatherNetworkManager> { get }
    func fetchCurrentWeather(cityName: String, completion: @escaping (CurrentWeatherResponse, Error?) -> Void )
    func fetchDailyWeather(cityName: String, completion: @escaping(DailyWeatherResponse, Error?) -> Void)
}

class WeatherNetworkService: NSObject, Networkable {

    static let shared = WeatherNetworkService()
    var weatherNetworkManager = MoyaProvider<WeatherNetworkManager>()

    func fetchCurrentWeather(cityName: String, completion: @escaping (CurrentWeatherResponse, Error?) -> Void ) {
        weatherNetworkManager.request(.currentWeather(cityName: cityName)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let currentWeather = try JSONDecoder().decode(CurrentWeatherResponse.self, from: response.data)
                    completion(currentWeather, nil)
                } catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    func fetchDailyWeather(cityName: String, completion: @escaping (DailyWeatherResponse, Error?) -> Void) {
        weatherNetworkManager.request(.fiveDailyWeather(cityName: cityName)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let dailyWeather = try JSONDecoder().decode(DailyWeatherResponse.self, from: response.data)
                    completion(dailyWeather, nil)
                } catch let error {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
