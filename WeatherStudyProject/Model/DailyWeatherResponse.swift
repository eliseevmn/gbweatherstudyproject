import Foundation

// MARK: - DailyWeatherResponse
struct DailyWeatherResponse: Decodable {
    let cod: String
    let message, cnt: Int
    let list: [DailyList]
    let city: DailyCity
}

// MARK: - City
struct DailyCity: Decodable {
    let idDaily: Int
    let name: String
    let coord: DailyCoord
    let country: String
    let population, timezone, sunrise, sunset: Int

    enum CodingKeys: String, CodingKey {
        case idDaily = "id"
        case name, coord, country, population, timezone, sunrise, sunset
    }
}

// MARK: - Coord
struct DailyCoord: Decodable {
    let lat, lon: Double
}

// MARK: - List
struct DailyList: Decodable {
    let date: Int
    let main: DailyMainClass
    let weather: [DailyWeather]
    let clouds: DailyClouds
    let wind: DailyWind
    let rain: DailyRain?
//    let sys: DailySys
    let dtTxt: String

    enum CodingKeys: String, CodingKey {
        case clouds, wind, rain
//        case sys
        case date = "dt"
        case main
        case weather
        case dtTxt = "dt_txt"
    }
}

// MARK: - Clouds
struct DailyClouds: Decodable {
    let all: Int
}

// MARK: - MainClass
struct DailyMainClass: Decodable {
    let temp, feelsLike, tempMin, tempMax: Double
    let pressure, seaLevel, grndLevel, humidity: Int
    let tempKf: Double

    enum CodingKeys: String, CodingKey {
        case temp
        case feelsLike = "feels_like"
        case tempMin = "temp_min"
        case tempMax = "temp_max"
        case pressure
        case seaLevel = "sea_level"
        case grndLevel = "grnd_level"
        case humidity
        case tempKf = "temp_kf"
    }
}

// MARK: - Rain
struct DailyRain: Decodable {
    let the3H: Double

    enum CodingKeys: String, CodingKey {
        case the3H = "3h"
    }
}

// MARK: - Sys
//struct DailySys: Decodable {
//    let pod: DailyPod
//}

//struct DailyPod: Decodable {
//    let dPod: String
//    let nPod: String
//
//    enum CodingKeys: String, CodingKey {
//        case dPod = "d"
//        case nPod = "n"
//    }
//}

// MARK: - Weather
struct DailyWeather: Decodable {
    let idDaily: Int
    let main: String
    let weatherDescription: String
    let icon: String

    enum CodingKeys: String, CodingKey {
        case idDaily = "id"
        case main
        case weatherDescription = "description"
        case icon
    }
}

enum DailyMain: String, Decodable {
    case clear = "Clear"
    case clouds = "Clouds"
    case rain = "Rain"
}

enum DailyDescription: String, Decodable {
    case brokenClouds = "broken clouds"
    case clearSky = "clear sky"
    case fewClouds = "few clouds"
    case lightRain = "light rain"
    case overcastClouds = "overcast clouds"
    case scatteredClouds = "scattered clouds"
}

// MARK: - Wind
struct DailyWind: Decodable {
    let speed: Double
    let deg: Int
}
